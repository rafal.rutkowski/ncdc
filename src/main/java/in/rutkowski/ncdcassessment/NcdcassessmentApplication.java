package in.rutkowski.ncdcassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NcdcassessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(NcdcassessmentApplication.class, args);
	}
}
