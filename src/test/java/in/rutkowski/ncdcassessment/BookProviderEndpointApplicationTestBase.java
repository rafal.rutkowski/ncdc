package in.rutkowski.ncdcassessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract public class BookProviderEndpointApplicationTestBase {
    @Autowired
    protected TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int port;

    private String getApplicationUrl(){
        return "http://localhost:" + port + "/";
    }

    protected String getAllBooksEndpointURL(){
        return getApplicationUrl() + "books-management/getAllBooks";
    }

    protected String getAddBookEndpointUrl(){
        return getApplicationUrl() + "books-management/addBook";
    }
}
