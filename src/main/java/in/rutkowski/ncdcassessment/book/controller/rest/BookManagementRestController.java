package in.rutkowski.ncdcassessment.book.controller.rest;

import in.rutkowski.ncdcassessment.book.model.data.BookData;
import in.rutkowski.ncdcassessment.book.model.data.BookDataListResponse;
import in.rutkowski.ncdcassessment.book.model.data.RESTOperationResultResponse;
import in.rutkowski.ncdcassessment.book.model.entity.BookEntity;
import in.rutkowski.ncdcassessment.book.model.repository.BookRepository;
import in.rutkowski.ncdcassessment.book.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/books-management")
public class BookManagementRestController {
    private final static Logger logger = LoggerFactory.getLogger(BookManagementRestController.class);

    @Autowired
    BookRepository bookRepository;
    @Autowired
    BookService bookService;


    @GetMapping("getAllBooks")
    public @ResponseBody BookDataListResponse getAllBooks(){
        List<BookData> bookDataList = bookService.getAllBooks();

        BookDataListResponse bookDataListResponse = new BookDataListResponse();
        bookDataListResponse.setBookDataList(bookDataList);

        return bookDataListResponse;
    }

    @PostMapping("addBook")
    public @ResponseBody RESTOperationResultResponse addBook(@Valid @RequestBody BookData bookData){
        RESTOperationResultResponse restOperationResultResponse = new RESTOperationResultResponse();

        if(logger.isDebugEnabled()){
            logger.debug("addBook data: " + bookData.toString());
        }

        try{
            BookEntity bookEntity = bookService.createBook(bookData);

            restOperationResultResponse.setResult(bookEntity.getId() != null);

            logger.info("New Book record created, id: " + bookEntity.getId());
        }catch (Exception e){
            restOperationResultResponse.setResult(false);
            restOperationResultResponse.setMessage("General exception while processing request");

            logger.error("Exception during addBook",e);
        }

        return restOperationResultResponse;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RESTOperationResultResponse handleValidation(MethodArgumentNotValidException e){
        return new RESTOperationResultResponse(e);
    }
}
