package in.rutkowski.ncdcassessment.book.service;

import in.rutkowski.ncdcassessment.book.model.data.BookData;
import in.rutkowski.ncdcassessment.book.model.entity.AuthorEntity;
import in.rutkowski.ncdcassessment.book.model.entity.BookEntity;
import in.rutkowski.ncdcassessment.book.model.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private ModelMapper modelMapper;

    @Transactional
    public BookEntity createBook(BookData bookData){
        BookEntity bookEntity = new BookEntity();

        AuthorEntity authorEntity = authorService.createOrUpdateAuthor(bookData);

        bookEntity.setAuthorEntity(authorEntity);

        modelMapper.map(bookData, bookEntity);

        return bookRepository.save(bookEntity);
    }

    public List<BookData> getAllBooks() {
        Iterable<BookEntity> bookEntitiesIterable = bookRepository.findAll();

        return StreamSupport.stream(bookEntitiesIterable.spliterator(), false)
                .map(bookEntity -> modelMapper.map(bookEntity, BookData.class))
                .collect(Collectors.toList());
    }
}
