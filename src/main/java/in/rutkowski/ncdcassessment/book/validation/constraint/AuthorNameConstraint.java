package in.rutkowski.ncdcassessment.book.validation.constraint;

import in.rutkowski.ncdcassessment.book.validation.validator.AuthorNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AuthorNameValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthorNameConstraint {
    String message() default "Invalid author name";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
