package in.rutkowski.ncdcassessment.book.model.repository;

import in.rutkowski.ncdcassessment.book.model.entity.AuthorEntity;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<AuthorEntity, Long> {
    AuthorEntity findByFullName(String fullName);
}
