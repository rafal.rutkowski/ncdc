package in.rutkowski.ncdcassessment.book.model.repository;

import in.rutkowski.ncdcassessment.book.model.entity.BookEntity;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<BookEntity, Long> {
    BookEntity findByISBN(String isbn);
}
