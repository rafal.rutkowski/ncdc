package in.rutkowski.ncdcassessment.book.controller.advice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class ModelAndViewDataControllerAdvice {
    @Value("${ncdcassestment.application.author}")
    private String applicationAuthor;
    @Value("${ncdcassestment.application.name}")
    private String applicationName;


    @ModelAttribute("applicationAuthor")
    public String getApplicationAuthor() {
        return applicationAuthor;
    }

    @ModelAttribute("applicationName")
    public String getApplicationName() {
        return applicationName;
    }
}
