package in.rutkowski.ncdcassessment.book.model.data;

import in.rutkowski.ncdcassessment.book.validation.constraint.AuthorNameConstraint;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BookData {
    @NotNull
    private String bookName;
    @NotNull
    private String bookISBN;
    @NotNull
    @AuthorNameConstraint
    private String authorName;
}
