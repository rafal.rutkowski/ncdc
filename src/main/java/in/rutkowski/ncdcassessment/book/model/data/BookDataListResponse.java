package in.rutkowski.ncdcassessment.book.model.data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BookDataListResponse {
    List<BookData> bookDataList = new ArrayList<>();
}
