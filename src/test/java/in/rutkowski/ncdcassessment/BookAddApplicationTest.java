package in.rutkowski.ncdcassessment;

import in.rutkowski.ncdcassessment.book.model.data.BookData;
import in.rutkowski.ncdcassessment.book.model.data.RESTOperationResultResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BookAddApplicationTest extends BookProviderEndpointApplicationTestBase {
    @Test
    public void performAddEmptyBookTest(){
        BookData bookData = new BookData();

        RESTOperationResultResponse result = this.testRestTemplate.postForObject(getAddBookEndpointUrl(), bookData, RESTOperationResultResponse.class);

        Assertions.assertFalse(result.getResult());
        Assertions.assertEquals(3, result.getFormValidationErrors().size());
    }


    @Test
    public void performAddBookWrongAuthorNameTest(){
        BookData bookData = new BookData();
        bookData.setBookISBN("isbn");
        bookData.setBookName("Book of Monsters");
        bookData.setAuthorName("Karol Testowy");

        RESTOperationResultResponse result = this.testRestTemplate.postForObject(getAddBookEndpointUrl(), bookData, RESTOperationResultResponse.class);

        Assertions.assertFalse(result.getResult());
        Assertions.assertEquals(result.getFormValidationErrors().size(), 1);
    }

    @Test
    public void performAddBookCorrectAuthorNameTest(){
        BookData bookData = new BookData();
        bookData.setBookISBN("isbn");
        bookData.setBookName("KBook of Monsters");
        bookData.setAuthorName("Asmodeusz Testowy");

        RESTOperationResultResponse result = this.testRestTemplate.postForObject(getAddBookEndpointUrl(), bookData, RESTOperationResultResponse.class);

        Assertions.assertTrue(result.getResult());
        Assertions.assertEquals(result.getFormValidationErrors().size(), 0);
    }


    @Test
    public void performAddBookCorrectAuthorName2Test(){
        BookData bookData = new BookData();
        bookData.setBookISBN("isbn");
        bookData.setBookName("Book of Monsters");
        bookData.setAuthorName("Boromir Atestowany");

        RESTOperationResultResponse result = this.testRestTemplate.postForObject(getAddBookEndpointUrl(), bookData, RESTOperationResultResponse.class);

        Assertions.assertTrue(result.getResult());
        Assertions.assertEquals(result.getFormValidationErrors().size(), 0);
    }
}
