package in.rutkowski.ncdcassessment.book.controller.web;

import in.rutkowski.ncdcassessment.book.model.data.BookData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BookController {
    @GetMapping("/")
    public ModelAndView index(){
        return new ModelAndView("index");
    }


    @GetMapping("/manage-book")
    public ModelAndView addBook(){
        ModelAndView modelAndView = new ModelAndView("book_manage");

        modelAndView.addObject("formModel", new BookData());

        return modelAndView;
    }
}
