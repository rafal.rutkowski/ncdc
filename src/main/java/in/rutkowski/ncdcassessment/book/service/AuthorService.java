package in.rutkowski.ncdcassessment.book.service;

import in.rutkowski.ncdcassessment.book.model.data.BookData;
import in.rutkowski.ncdcassessment.book.model.entity.AuthorEntity;
import in.rutkowski.ncdcassessment.book.model.repository.AuthorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {
    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    ModelMapper modelMapper;

    public AuthorEntity createOrUpdateAuthor(BookData bookData){
        AuthorEntity authorEntity = authorRepository.findByFullName(bookData.getAuthorName());

        if(authorEntity == null){
            authorEntity = new AuthorEntity();
        }


        modelMapper.map(bookData, authorEntity);

        return authorRepository.save(authorEntity);
    }
}
