package in.rutkowski.ncdcassessment.book.model.data;

import lombok.Data;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.HashMap;
import java.util.Map;

@Data
public class RESTOperationResultResponse {
    Boolean result;
    String message;
    final Map<String, String> formValidationErrors = new HashMap<>();

    public RESTOperationResultResponse(MethodArgumentNotValidException e){
        result = false;

        e.getBindingResult().getAllErrors().forEach( c -> {
            FieldError fieldError = (FieldError) c;
            formValidationErrors.put(fieldError.getField(), c.getDefaultMessage());
        });
    }

    public RESTOperationResultResponse() {

    }
}
