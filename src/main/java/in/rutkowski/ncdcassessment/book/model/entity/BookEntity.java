package in.rutkowski.ncdcassessment.book.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "book")
public class BookEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotBlank
    private String ISBN;

    @Column
    @NotBlank
    private String name;

    @ManyToOne(optional = false)
    @JoinColumn(name = "author_id")
    private AuthorEntity authorEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AuthorEntity getAuthorEntity() {
        return authorEntity;
    }

    public void setAuthorEntity(AuthorEntity authorRecord) {
        this.authorEntity = authorRecord;
    }
}
