package in.rutkowski.ncdcassessment.book.validation.validator;

import in.rutkowski.ncdcassessment.book.validation.constraint.AuthorNameConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthorNameValidator implements ConstraintValidator<AuthorNameConstraint, String> {
    @Override
    public void initialize(AuthorNameConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String authorName, ConstraintValidatorContext constraintValidatorContext) {
        if(authorName == null){
            return true;
        }

        Pattern pattern = Pattern.compile("(^A|\\ A)");
        Matcher matcher = pattern.matcher(authorName);

        return matcher.find();
    }
}
