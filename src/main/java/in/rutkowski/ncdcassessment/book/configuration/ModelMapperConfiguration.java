package in.rutkowski.ncdcassessment.book.configuration;

import in.rutkowski.ncdcassessment.book.model.data.BookData;
import in.rutkowski.ncdcassessment.book.model.entity.AuthorEntity;
import in.rutkowski.ncdcassessment.book.model.entity.BookEntity;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfiguration {
    @Bean
    ModelMapper modelMapper(){
        ModelMapper modelMapper = new ModelMapper();

        configureModelMapper(modelMapper);

        return modelMapper;
    }

    private void configureModelMapper(ModelMapper modelMapper) {
        modelMapper.getConfiguration().setImplicitMappingEnabled(false);

        modelMapper.typeMap(BookData.class, BookEntity.class)
                .addMapping(BookData::getBookISBN, BookEntity::setISBN)
                .addMapping(BookData::getBookName, BookEntity::setName);

        modelMapper.typeMap(BookData.class, AuthorEntity.class)
                .addMapping(BookData::getAuthorName, AuthorEntity::setFullName);

        modelMapper.typeMap(BookEntity.class, BookData.class)
                .addMapping(bookEntity -> bookEntity.getAuthorEntity().getFullName(), BookData::setAuthorName)
                .addMapping(BookEntity::getISBN, BookData::setBookISBN)
                .addMapping(BookEntity::getName, BookData::setBookName);
    }
}
