package in.rutkowski.ncdcassessment;

import in.rutkowski.ncdcassessment.book.model.data.BookData;
import in.rutkowski.ncdcassessment.book.model.data.BookDataListResponse;
import in.rutkowski.ncdcassessment.book.model.data.RESTOperationResultResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookListProviderApplicationTest extends BookProviderEndpointApplicationTestBase {
	@Test
	void performContextLoads() {
		BookData bookData = new BookData();
		String isbn = "isbn";

		bookData.setBookISBN(isbn);
		bookData.setBookName("Book of Monsters");
		bookData.setAuthorName("Boromir Atestowany");

		RESTOperationResultResponse result = this.testRestTemplate.postForObject(getAddBookEndpointUrl(), bookData, RESTOperationResultResponse.class);

		Assertions.assertTrue(result.getResult());

		BookDataListResponse bookDataListResponse = testRestTemplate.getForObject(getAllBooksEndpointURL(), BookDataListResponse.class);

		Assertions.assertTrue(bookDataListResponse.getBookDataList().size() > 0);

	}

}
