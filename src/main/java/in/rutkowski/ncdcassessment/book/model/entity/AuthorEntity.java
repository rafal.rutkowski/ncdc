package in.rutkowski.ncdcassessment.book.model.entity;

import in.rutkowski.ncdcassessment.book.validation.constraint.AuthorNameConstraint;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name = "author")
public class AuthorEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "full_name", unique = true)
    @NotBlank
    @AuthorNameConstraint
    private String fullName;


    @OneToMany
    @JoinColumn(name = "author_id")
    private List<BookEntity> bookRecords;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<BookEntity> getBookRecords() {
        return bookRecords;
    }

    public void setBookRecords(List<BookEntity> bookRecords) {
        this.bookRecords = bookRecords;
    }
}
